<?php

/**
 * @file
 * Install file for rfn profile.
 */

use Drupal\shortcut\Entity\Shortcut;

/**
 * @file
 * Install, update and uninstall functions for RFN.
 */

/**
 * Implements hook_install().
 */
function rfn_profile_core_install() {
  \Drupal::configFactory()->getEditable('system.theme')
    ->set('default', 'claro')->save();

  // Set up default shortcuts.
  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('Browse Artists'),
    'weight' => -20,
    'link' => [
      'uri' => 'internal:/browse/artists',
    ],
  ]);
  $shortcut->save();

  $shortcut = Shortcut::create([
    'shortcut_set' => 'default',
    'title' => t('Browse Albums'),
    'weight' => -20,
    'link' => [
      'uri' => 'internal:/browse/albums',
    ],
  ]);
  $shortcut->save();

}

/**
 * Implements hook_uninstall().
 */
function rfn_profile_core_uninstall() {

}

/**
 * Implements hook_schema().
 */
function rfn_profile_core_schema() {
  $schema['rfn_profile_core_example'] = [
    'description' => 'Table description.',
    'fields' => [
      'id' => [
        'type' => 'serial',
        'not null' => TRUE,
        'description' => 'Primary Key: Unique record ID.',
      ],
      'uid' => [
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'description' => 'The {users}.uid of the user who created the record.',
      ],
      'status' => [
        'description' => 'Boolean indicating whether this record is active.',
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
        'default' => 0,
        'size' => 'tiny',
      ],
      'type' => [
        'type' => 'varchar_ascii',
        'length' => 64,
        'not null' => TRUE,
        'default' => '',
        'description' => 'Type of the record.',
      ],
      'created' => [
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Timestamp when the record was created.',
      ],
      'data' => [
        'type' => 'blob',
        'not null' => TRUE,
        'size' => 'big',
        'description' => 'The arbitrary data for the item.',
      ],
    ],
    'primary key' => ['id'],
    'indexes' => [
      'type' => ['type'],
      'uid' => ['uid'],
      'status' => ['status'],
    ],
  ];

  return $schema;
}

/**
 * Implements hook_requirements().
 */
function rfn_profile_core_requirements($phase) {
  $requirements = [];

  if ($phase == 'runtime') {
    $value = mt_rand(0, 100);
    $requirements['rfn_profile_core_status'] = [
      'title' => t('Radio Free Network Core Install Profile status'),
      'value' => t('Radio Free Network Core Install Profile value: @value', ['@value' => $value]),
      'severity' => $value > 50 ? REQUIREMENT_INFO : REQUIREMENT_WARNING,
    ];
  }

  return $requirements;
}

/**
 * Implements hook_modules_installed().
 */
function rfn_modules_installed($modules) {

  \Drupal::configFactory()
    ->getEditable('system.theme')
    ->set('default', 'claro')
    ->save();

}
